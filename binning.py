import numpy as np
import os
import cv2
import math
import random

def binning(a, b = 1, sumStack = True, good = 2520):
   """Given two- or three-dimensional numpy array a and the size of a bin b,
      returns a binned numpy array.  The depth is summed together if sumStack
      is True.  The size of a bin can be 1-10 as long as the default value of
      good is 2520.  good must be multiple of other bin size if desired.
   """
   assert good % b == 0, "Supply a multiple of the bin size."
   if len(a.shape) == 3: depth, height, width = a.shape
   else:
      depth = 1
      height, width = a.shape
      sumStack = True
   H = good * int(np.ceil(float(height)/good))
   W = good * int(np.ceil(float(width) /good))
   z = np.zeros([depth, H, W], dtype = a.dtype)
   z[:, :height, :width] = a
   a = z.reshape(depth, int(H/b), b, int(W/b), b).sum(4).sum(2)
   if sumStack:
      a = a.sum(0)
      return a[:int(height/b), :int(width/b)]
   else:
      return a[:, :int(height/b), :int(width/b)]

os.chdir("C:/Users/micha/Dropbox/")
cur_path = os.getcwd()
imageFolder = os.path.abspath(cur_path+'/images')
os.chdir("C:/Users/micha/Documents/Default Directories Are Dumb/Projects/machine-learning-practice")
cur_path = os.getcwd()
b = 8

failed = open(cur_path + "/failed.txt", "w")
start = 4000
counter = 0

for _,_,files in os.walk(imageFolder):
   x = len(files)
   images = [0]*x
   for name in enumerate(files):
      if name[1][-5:] == ".mccd" and counter > start:
         imagePath = os.path.abspath(imageFolder+'/'+name[1])
         # print(imagePath)
         # print(np.sum(cv2.imread(imagePath,cv2.IMREAD_UNCHANGED)>255))

         try:
            temp = binning(cv2.imread(imagePath,cv2.IMREAD_UNCHANGED),b)
            std = np.std(temp)
         except:
            print(imagePath)
            failed.write(imagePath + "\n")

         # mi,ma = bkgdScattering(temp,0)
         # temp = (temp-mi)*255/(ma-mi)
         temp = (temp-np.min(temp))*255/(std*3-np.min(temp))
         images[name[0]] = temp

         cv2.imwrite("%s/output/%s-binned.jpg" % (cur_path,name[1]), images[name[0]])
      counter += 1

failed.close