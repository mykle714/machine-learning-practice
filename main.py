from sklearn.linear_model import SGDClassifier
import os
import numpy as np
import cv2
import pandas as pd
import pickle

def getGoodImages():
	os.chdir("C:/Users/micha/Documents/Default Directories Are Dumb/Projects/machine-learning-practice")
	cur_path = os.getcwd()
	rankingsPath = os.path.abspath(cur_path+'/rankings.csv')

	df = pd.read_csv(rankingsPath)

	nparr = df.to_numpy()

	ret = nparr[2]

	return ret

def dataLoader(start, batchSize):
	os.chdir("C:/Users/micha/Documents/Default Directories Are Dumb/Projects/machine-learning-practice")
	cur_path = os.getcwd()
	imageFolder = os.path.abspath(cur_path+'/output')
	goodImages = getGoodImages()
	print(goodImages)

	for _,_,files in os.walk(imageFolder):
		flatLen = np.power(len(cv2.imread("%s/%s" % (imageFolder, files[0]),0)),2)
		print(flatLen, len(files))
		ret1 = np.zeros((flatLen,len(files)))
		ret2 = np.zeros((flatLen,len(files)))
		# ret1 = np.array([np.zeros(flatLen)]*len(files))
		# ret2 = np.array([np.zeros(flatLen)]*len(files))

		for i in range(start,start+len(batchSize)):
			tempimg = cv2.imread("%s/%s" % (imageFolder, files[i]),0)
			ret1[i] = np.ndarray.flatten(tempimg)
			if(files[i].split("-")[0] in goodImages):
				ret2[i] = 1

	return ret1, ret2

def train(X, Y):
	loss = "hinge" #loss : str, ‘hinge’, ‘log’, ‘modified_huber’, ‘squared_hinge’, ‘perceptron’, or a regression loss: ‘squared_loss’, ‘huber’, ‘epsilon_insensitive’, or ‘squared_epsilon_insensitive’
	model = SGDClassifier(loss=loss, shuffle=True)

	model.partial_fit(X, Y)

	os.chdir("C:/Users/micha/Documents/Default Directories Are Dumb/Projects/machine-learning-practice")
	cur_path = os.getcwd()
	with open(cur_path + "pickle","wb") as f:
		pickle.dump(model,f)
	return model

def test(model, X, Y):
	results1 = model.predict(X) - Y
	results2 = model.predict(X) + Y

	tp = sum(results2==2)
	fp = sum(results1==1)
	fn = sum(results1==-1)
	tn = sum(results2==0)

	precision = tp/(tp+fp)
	recall = tp/(tp+fn)
	accuracy = (tp+tn)/len(Y)
	f1score = 2*(precision*recall)/(precision+recall)

	return tp, fp, fn, tn, precision, recall, accuracy, f1score

def predict(model, X):
	return model.predict(X)


data, labels = dataLoader()

model = train(data, labels)

results = test(model, data, labels)

print(results)